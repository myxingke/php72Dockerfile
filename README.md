# php72Dockerfile

#### 项目介绍
依赖：php:7.2-fpm-alpine3.8 构建的一个轻小的php环境镜像

#### 使用方式
```
git clone https://gitee.com/myxingke/php72Dockerfile.git
docker build -t php72:v1 php72Dockerfile
```

[hub地址](https://hub.docker.com/repository/docker/5353920/php72 "hub地址")

#### 已安装扩展
bcmath \
calendar \
Core \
ctype \
curl \
date \
dom \
exif \
fileinfo \
filter \
ftp \
gd \
gettext \
hash \
iconv \
json \
libxml \
mbstring \
memcached \
mongodb \
mysqli \
mysqlnd \
openssl \
pcntl \
pcre \
PDO \
pdo_mysql \
pdo_pgsql \
pdo_sqlite \
Phar \
posix \
readline \
redis \
Reflection \
session \
shmop \
SimpleXML \
soap \
sockets \
sodium \
SPL \
sqlite3 \
standard \
swoole \
sysvmsg \
sysvsem \
sysvshm \
tokenizer \
wddx \
xdebug \
xml \
xmlreader \
xmlrpc \
xmlwriter \
xsl \
Zend OPcache \
zip \
zlib \
xhprof \
[Zend Modules] \
Xdebug \
Zend OPcache

## 更多扩展请运行看查看 phpinfo()

### docker push 异常
报了denied: requested access to the resource is denied异常
需要使用 docker tag改名字
```$xslt
docker tag 3bd2787b9fa3 5353920/php72:v1
```
然后再push
```$xslt
docker push 5353920/php72:v1
```